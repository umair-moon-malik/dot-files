#!/bin/bash

notify-send -t 0 "Time: $(date +'%I:%M %p')" "Date: $(date +'%d %B %Y, %A')\nBattery Percentage: $(acpi | cut -d, -f2 | tr -d ' ')"
