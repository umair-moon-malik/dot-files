if status is-interactive
    # Commands to run in interactive sessions can go here
end
export QT_QPA_PLATFORMTHEME="qt5ct"

set -U fish_greeting


function fix-audio
    sudo modprobe -r snd_sof_pci_intel_tgl
    sudo modprobe snd_sof_pci_intel_tgl
end

alias fix-my-audio="fix-audio"



alias mc 'java -jar /home/umair-malik/Downloads/tlauncher/TLauncher-2.885.jar'
alias steam 'flatpak run com.valvesoftware.Steam'
alias bt 'blueman-manager'
alias nm 'nm-connection-editor'


export QT_QPA_PLATFORM=wayland
export XDG_CURRENT_DESKTOP=sway
export XDG_SESSION_DESKTOP=sway
export XDG_SESSION_DESKTOP_TYPE=wayland
export GDK_BACKEND="wayland,x11"
export MOZ_ENABLE_WAYLAND=1


set -g theme_display_vi no
set -g theme_display_date no
set -g theme_title_display_user no
set -g theme_color_scheme terminal-dark-black

set -x XDG_CURRENT_DESKTOP "Hyprland"
set -x DESKTOP_SESSION "Hyprland"

colorscript exec crunchbang-mini
neofetch
tput bold; date +"%I:%M %p"; tput sgr0; tput bold; date +"%d %B %Y , %A"; tput sgr0
